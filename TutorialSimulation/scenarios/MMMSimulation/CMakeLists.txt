
set(SCENARIO_CONFIG_COMPONENTS

	config/DebugObserver.cfg
	config/ConditionHandler.cfg
	config/SystemObserver.cfg

	config/CommonStorage.cfg
	config/PriorKnowledge.cfg
	config/LongtermMemory.cfg
	config/WorkingMemory.cfg

	config/RobotStateComponent.cfg
	config/RobotIK.cfg
	config/SimulatorApp.cfg
	config/SimulatorViewerApp.cfg

	config/KinematicUnitDynamicSimulationApp.cfg
	config/KinematicUnitObserver.cfg
	config/ObjectMemoryObserver.cfg
	config/SelfLocalizationDynamicSimulationApp.cfg

	config/HeadIKUnit.cfg
	config/TCPControlUnit.cfg
)
#	config/ViewSelectionApp.cfg
#	config/PlatformUnitDynamicSimulationApp.cfg
#	config/PlatformUnitObserver.cfg
#	config/AStarPathPlanner.cfg
#	config/ImageProviderDynamicSimulationApp.cfg
#	config/ObjectLocalizationDynamicSimulationApp.cfg
#	config/ObjectLocalizationDynamicSimulationApp.SegmentableRecognition.cfg
#	config/ForceTorqueUnitDynamicSimulationApp.cfg
#	config/ForceTorqueObserver.cfg
#	config/RobotHandLocalizationDynamicSimulationApp.cfg
#	config/GraphNodePoseResolverApp.cfg
#	config/HandUnitDynamicSimulationApp.LeftHand.cfg
#	config/HandUnitDynamicSimulationApp.RightHand.cfg

# optional:
#	config/ArmarXGui.cfg


# optional 3rd parameter: "path/to/global/config.cfg"
armarx_scenario_from_configs("MMMSimulation" "${SCENARIO_CONFIG_COMPONENTS}" "config/global.cfg")

