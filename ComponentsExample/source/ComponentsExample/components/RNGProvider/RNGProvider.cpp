/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ComponentsExample::ArmarXObjects::RNGProvider
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RNGProvider.h"


using namespace armarx;


void RNGProvider::onInitComponent()
{
    std::time_t now = std::time(nullptr);
    gen = boost::random::mt19937{static_cast<std::uint32_t>(now)};
}


void RNGProvider::onConnectComponent()
{
    ARMARX_IMPORTANT << "RNG output: " << generateRandomInt();
}


void RNGProvider::onDisconnectComponent()
{

}


void RNGProvider::onExitComponent()
{

}

armarx::PropertyDefinitionsPtr RNGProvider::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new RNGProviderPropertyDefinitions(
            getConfigIdentifier()));
}

Ice::Int RNGProvider::generateRandomInt(const Ice::Current&)
{
    return gen();
}

