/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ComponentsExample::ArmarXObjects::RNGProvider
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_ComponentsExample_RNGProvider_H
#define _ARMARX_COMPONENT_ComponentsExample_RNGProvider_H


#include <ArmarXCore/core/Component.h>

#include <ComponentsExample/interface/RNGComponentProviderIceInterface.h>
#include <boost/random.hpp>

namespace armarx
{
    /**
     * @class RNGProviderPropertyDefinitions
     * @brief
     */
    class RNGProviderPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        RNGProviderPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @defgroup Component-RNGProvider RNGProvider
     * @ingroup ComponentsExample-Components
     * A description of the component RNGProvider.
     *
     * @class RNGProvider
     * @ingroup Component-RNGProvider
     * @brief Brief description of class RNGProvider.
     *
     * Detailed description of class RNGProvider.
     */
    class RNGProvider :
        virtual public armarx::Component,
        virtual public armarx::RNGProviderComponentInterface
    {
    private:
        boost::random::mt19937 gen;

    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "RNGProvider";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

        // RNGProviderComponentInterface interface
    public:
        Ice::Int generateRandomInt(const Ice::Current& c = Ice::emptyCurrent) override;
    };
}

#endif
