/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ComponentsExample::ArmarXObjects::RNGCaller
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "RNGCaller.h"


using namespace armarx;


void RNGCaller::onInitComponent()
{
    usingProxy(getProperty<std::string>("RNGProviderName").getValue());
}


void RNGCaller::onConnectComponent()
{
    interfacePrx = getProxy<RNGProviderComponentInterfacePrx>(getProperty<std::string>("RNGProviderName").getValue());
    ARMARX_IMPORTANT << "RNGProvider component output over Ice:  " << interfacePrx->generateRandomInt();
}


void RNGCaller::onDisconnectComponent()
{

}


void RNGCaller::onExitComponent()
{

}

armarx::PropertyDefinitionsPtr RNGCaller::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new RNGCallerPropertyDefinitions(
            getConfigIdentifier()));
}

