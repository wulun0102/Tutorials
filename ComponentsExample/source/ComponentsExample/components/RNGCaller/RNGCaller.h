/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    ComponentsExample::ArmarXObjects::RNGCaller
 * @author     Fabian Paus ( fabian dot paus at kit dot edu )
 * @date       2016
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_ComponentsExample_RNGCaller_H
#define _ARMARX_COMPONENT_ComponentsExample_RNGCaller_H


#include <ArmarXCore/core/Component.h>

#include <ComponentsExample/interface/RNGComponentProviderIceInterface.h>

namespace armarx
{
    /**
     * @class RNGCallerPropertyDefinitions
     * @brief
     */
    class RNGCallerPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        RNGCallerPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");

            defineOptionalProperty<std::string>("RNGProviderName",
                                                "RNGProvider",
                                                "Name of the component that offers the RNG");
        }
    };

    /**
     * @defgroup Component-RNGCaller RNGCaller
     * @ingroup ComponentsExample-Components
     * A description of the component RNGCaller.
     *
     * @class RNGCaller
     * @ingroup Component-RNGCaller
     * @brief Brief description of class RNGCaller.
     *
     * Detailed description of class RNGCaller.
     */
    class RNGCaller :
        virtual public armarx::Component
    {
    private:
        RNGProviderComponentInterfacePrx interfacePrx;

    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "RNGCaller";
        }

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;
    };
}

#endif
