/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package
 * @author
 * @date
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */
#define BOOST_TEST_MODULE ArmarX::Tutorials::WavingTutorialScenarioTestExample
#define ARMARX_BOOST_TEST

#include <WavingTutorial/Test.h>

// Includes the environment for testing whole statechart-scenarios
#include <ArmarXCore/statechart/test/StatechartScenarioTestEnv.h>

#include <boost/test/floating_point_comparison.hpp>

using namespace armarx;

BOOST_AUTO_TEST_CASE(WavingTutorialScenarioTestExample)
{
    // Initialising the testing-environment.
    StatechartScenarioTestEnvironment env("WavingTutorialScenarioTestExample");

    ///// Tests can depend on multiple scenarios - usually the simulation + x
    // This scenario sets up the simulation for ArmarX-Robots, it should be started before any other robot-related
    // scenario.
    // By passing the third argument, we let the environment wait until that given component is started (or 5000ms
    // have passed), to prevent situations where the simulation is not set up. The component 'Simulator' has
    // proven to be the best choice.
    // env.startScenario("ArmarXSimulation", "Armar3Simulation", "Simulator", 5000); //<-- this simple test case does not need the simulation

    // Now we start the scenario we want to test
    env.startScenario("WavingTutorial", "StartStateChart");

    // Since we cannot detect if a statechart gets stuck in an infinite loop or something similar,
    // we set a maximal execution time (in milliseconds).
    // If this time is over and the statechart has not finished, an exception is thrown and therefore
    // the whole test fails.
    env.watcher->waitForStateChartFinished(90000);

    // After the statechart has finished, we can extract the output parameters of the main-state
    // of the tested statechart and test these parameters afterwards
    StringVariantContainerBaseMap statechartOutput = env.watcher->getStatechartOutput();

    // Extracting the actual result values of the output parameters.
    VariantContainerBasePtr counterResult = statechartOutput.find("counterResult")->second;
    VariantPtr counterResultVariant = (SingleVariantPtr::dynamicCast(counterResult))->get();
    int counterResultValue = counterResultVariant->getInt();

    VariantContainerBasePtr finalJointValues = statechartOutput.find("FinalJointValues")->second;
    VariantPtr elbowLVariant = (StringValueMapPtr::dynamicCast(finalJointValues))->getVariant("Elbow L");
    float elbowLValue = elbowLVariant->getFloat();

    // Example-tests
    // -----------------------
    // Expected results:
    // counterResult -> 5
    // FinalJointValues -> "ElbowL" -> 0.3300000131130219 (the value of the input parameter 'JointMapValueWaveBack' -> "Elbow L")

    BOOST_CHECK_EQUAL(counterResultValue, 5);

    float expectedElbowLValue = 0.3300000131130219;
    float eps = 0.06; // absolute in radiant

    BOOST_CHECK_SMALL(elbowLValue - expectedElbowLValue, eps);
    env.manager->removeObjectBlocking("ProfilerObserver");
    ARMARX_INFO << "DONE";
}
