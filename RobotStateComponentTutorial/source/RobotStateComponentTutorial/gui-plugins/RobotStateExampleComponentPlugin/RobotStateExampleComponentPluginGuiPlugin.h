/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotStateComponentTutorial::gui-plugins::RobotStateExampleComponentPlugin
 * @author     Martin Do ( martin dot do at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_RobotStateComponentTutorial_RobotStateExampleComponentPlugin_GuiPlugin_H
#define _ARMARX_RobotStateComponentTutorial_RobotStateExampleComponentPlugin_GuiPlugin_H

#include <ArmarXCore/core/system/ImportExportComponent.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXGuiPlugin.h>
#include <ArmarXGui/libraries/ArmarXGuiBase/ArmarXComponentWidgetController.h>

namespace armarx
{
    /**
     * @class RobotStateExampleComponentPluginGuiPlugin
     * @ingroup ArmarXGuiPlugins
     * @brief RobotStateExampleComponentPluginGuiPlugin brief description
     *
     * Detailed description
     */
    class ARMARXCOMPONENT_IMPORT_EXPORT RobotStateExampleComponentPluginGuiPlugin:
        public armarx::ArmarXGuiPlugin
    {
        Q_OBJECT
#if QT_VERSION >= QT_VERSION_CHECK(5, 0, 0)
        Q_INTERFACES(ArmarXGuiInterface)
        Q_PLUGIN_METADATA(IID "ArmarXGuiInterface/1.00")
#endif
    public:
        /**
         * All widgets exposed by this plugin are added in the constructor
         * via calls to addWidget()
         */
        RobotStateExampleComponentPluginGuiPlugin();
    };
}

#endif
