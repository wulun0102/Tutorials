/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2014-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotStateComponentTutorial::ArmarXObjects::RobotStateExampleComponent
 * @author     Martin Do ( martin dot do at kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#ifndef _ARMARX_COMPONENT_RobotStateComponentTutorial_RobotStateExampleComponent_H
#define _ARMARX_COMPONENT_RobotStateComponentTutorial_RobotStateExampleComponent_H

#include <RobotAPI/libraries/core/remoterobot/RemoteRobot.h>
#include <RobotAPI/interface/visualization/DebugDrawerInterface.h>
#include <RobotAPI/libraries/core/FramedPose.h>
#include <ArmarXCore/core/services/tasks/PeriodicTask.h>
#include <ArmarXCore/core/Component.h>
#include <RobotStateComponentTutorial/interface/RobotStateExampleComponentInterface.h>

namespace armarx
{
    /**
     * @class RobotStateExampleComponentPropertyDefinitions
     * @brief
     */
    class RobotStateExampleComponentPropertyDefinitions:
        public armarx::ComponentPropertyDefinitions
    {
    public:
        RobotStateExampleComponentPropertyDefinitions(std::string prefix):
            armarx::ComponentPropertyDefinitions(prefix)
        {
            //defineRequiredProperty<std::string>("PropertyName", "Description");
            //defineOptionalProperty<std::string>("PropertyName", "DefaultValue", "Description");
        }
    };

    /**
     * @class RobotStateExampleComponent
     * @ingroup RobotStateComponentTutorial-Components
     * @brief A brief description
     *
     *
     * Detailed Description
     */
    class RobotStateExampleComponent :
        virtual public armarx::RobotStateExampleComponentInterface,
        virtual public armarx::Component
    {
    public:
        /**
         * @see armarx::ManagedIceObject::getDefaultName()
         */
        std::string getDefaultName() const override
        {
            return "RobotStateExampleComponent";
        }
        /**
             * Allows to select the robot node, identified by name, whose coordinate systems is to be displayed.
             */
        void setSelectedRobotNodeName(const std::string& rnName, const Ice::Current& c = Ice::emptyCurrent) override;
        /**
             * Allows to set the position of an abritrary task space position in global coordinate system.
             */
        void setTargetPosition(const Vector3BasePtr& newTarget, const Ice::Current& c = Ice::emptyCurrent) override;
        /**
             * Enables/disables the displaying of the selected robot node coordinate system
             */
        void setShowCoordinateSystem(bool bShowCoordSys, const Ice::Current& c = Ice::emptyCurrent) override;
        /**
             * Enables/disables the displaying of an arrow pointing from the selected robot node to the set task space position
             */
        void setShowTargetVector(bool bShowTargetVec, const Ice::Current& c = Ice::emptyCurrent) override;
        /**
             * Enables/disables the displaying of a sphere surrounding the set task space position
             */
        void setShowTargetPosition(bool bShowTargetPos, const Ice::Current& c = Ice::emptyCurrent) override;
        /**
             * Returns all robot nodes of the RemoteRobot encoded in the RobotStateComponent
             */
        std::vector<std::string> getRobotNodeNames(const Ice::Current& c = Ice::emptyCurrent) override;

    protected:
        /**
         * @see armarx::ManagedIceObject::onInitComponent()
         */
        void onInitComponent() override;

        /**
         * @see armarx::ManagedIceObject::onConnectComponent()
         */
        void onConnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onDisconnectComponent()
         */
        void onDisconnectComponent() override;

        /**
         * @see armarx::ManagedIceObject::onExitComponent()
         */
        void onExitComponent() override;

        /**
         * @see PropertyUser::createPropertyDefinitions()
         */
        armarx::PropertyDefinitionsPtr createPropertyDefinitions() override;

    private:
        /**
             * Enables the DebugDrawer to draw the joint coordinate system of the robot node with the name
         * robotNodeName.
             */
        void showRobotNodePoseCoordinateSystem(std::string robotNodeName, bool bShow);
        /**
             * Enables the DebugDrawer to draw an arrow from  joint coordinate system of the robot node with the name
         * robotNodeName to the given targetPosition. The targetPosition in global frame is transformed into local
         * coordinate system in the robot node. The targetVector is calculate in local coordinate system and
         * transformed back into global frame.
             */
        void showTargetVector(std::string robotNodeName, FramedPositionPtr targetPosition, bool bShow);
        /**
             * Enables the DebugDrawer to draw a sphere at the targetPosition in global frame.
             */
        void showTargetPosition(FramedPositionPtr targetPosition, bool bShow);
        /**
             * This method is executed periodically and executes showRobotNodePoseCoordinateSystem, showTargetVector, showTargetPosition.
             */
        void periodicExec();
        /**
             * Thread object which runs periodicExec() with the cycleTime in ms
             */
        armarx::PeriodicTask<RobotStateExampleComponent>::pointer_type execTask;
        int cycleTime;

        /**
             * Proxy to the RobotStateComponent
             */
        RobotStateComponentInterfacePrx robotStateComponentPrx;
        /**
             * local VirtualRobot object used to clone the RemoteRobot encoded in RobotStateComponent
             */
        VirtualRobot::RobotPtr localRobot;
        /**
             * Proxy to DebugDrawer
             */
        DebugDrawerInterfacePrx drawer;
        /**
             * Mutex to control access of the RobotStateExampleComponent
             */
        std::mutex accessMutex;
        /**
             * Member variable for selectedRobotNodeName
             */
        std::string selectedRobotNodeName;
        /**
             * Member variable for targetPositionGlobal
             */
        FramedPositionPtr targetPositionGlobal;
        /**
             * Boolean stating if component has started
             */
        bool bStarted;
        /**
             * Boolean indicating whether joint coordinate system should be shown
             */
        bool bShowCoordinateSystem;
        /**
             * Boolean indicating whether targetVector (arrow) should be shown
             */
        bool bShowTargetVector;
        /**
             * Boolean indicating whether targetPosition (sphere) should be shown
             */
        bool bShowTargetPosition;
    };
}

#endif
