
# Libs required for the tests
SET(LIBS ${LIBS} ArmarXCore RobotStateExampleComponent)
 
armarx_add_test(RobotStateExampleComponentTest RobotStateExampleComponentTest.cpp "${LIBS}")