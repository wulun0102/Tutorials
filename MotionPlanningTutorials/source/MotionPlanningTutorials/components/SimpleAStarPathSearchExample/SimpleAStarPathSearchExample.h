/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MotionPlanningTutorials
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#ifndef _ARMARX_COMPONENT_MotionPlanningTutorials_SimpleAStarPathSearchExampleComponent_H
#define _ARMARX_COMPONENT_MotionPlanningTutorials_SimpleAStarPathSearchExampleComponent_H

#include <ArmarXCore/core/Component.h>
#include <MemoryX/interface/components/WorkingMemoryInterface.h>

#include <ArmarXCore/core/util/distributed/RemoteHandle/RemoteHandle.h>

#include <RobotComponents/interface/components/MotionPlanning/MotionPlanningServer.h>
#include <RobotComponents/components/MotionPlanning/Tasks/AStar/Task.h>

namespace armarx
{
    class SimpleAStarPathSearchExampleComponent;

    typedef IceInternal::Handle<SimpleAStarPathSearchExampleComponent> ExamplePlanningComponentPtr;

    class SimpleAStarPathSearchExampleComponentPropertyDefinitions:
        public ComponentPropertyDefinitions
    {
    public:
        SimpleAStarPathSearchExampleComponentPropertyDefinitions(std::string prefix):
            ComponentPropertyDefinitions(prefix)
        {
            defineOptionalProperty<std::string>("PlanningServerName", "MotionPlanningServer", "The planning server's name.");
            defineOptionalProperty<std::string>("WorkingMemoryName", "WorkingMemory", "The used working memory. (for objects)");
        }
    };

    class SimpleAStarPathSearchExampleComponent:
        virtual public Component
    {
    public:
        /**
         * @brief ctor
         */
        SimpleAStarPathSearchExampleComponent() = default;
        /**
         * @brief dtor
         */
        ~SimpleAStarPathSearchExampleComponent() override = default;

        // inherited from Component
        std::string getDefaultName() const override
        {
            return "SimpleAStarPathSearchExampleComponent";
        }
        void onInitComponent() override;
        void onConnectComponent() override;
        void onDisconnectComponent() override {}
        void onExitComponent() override {}

        PropertyDefinitionsPtr createPropertyDefinitions() override
        {
            return PropertyDefinitionsPtr(new SimpleAStarPathSearchExampleComponentPropertyDefinitions(getConfigIdentifier()));
        }

    protected:
        std::string pServerName;
        std::string wMemName;

        memoryx::WorkingMemoryInterfacePrx wMemProxy;
        MotionPlanningServerInterfacePrx pServerProxy;
        AStarTaskHandle astarHandle;
    };
}
#endif
