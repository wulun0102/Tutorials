/*
 * This file is part of ArmarX.
 *
 * Copyright (C) 2011-2016, High Performance Humanoid Technologies (H2T), Karlsruhe Institute of Technology (KIT), all rights reserved.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    MotionPlanningTutorials
 * @author     Raphael Grimm ( ufdrv at student dot kit dot edu )
 * @date       2015
 * @copyright  http://www.gnu.org/licenses/gpl.txt
 *             GNU General Public License
 */
#include <RobotAPI/libraries/core/Pose.h>

#include "MotionPlanningKitchenExample.h"

#include <exception>
#include <algorithm>
#include <cmath>
#include <unordered_map>

#include <RobotAPI/libraries/core/Pose.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectClass.h>
#include <MemoryX/libraries/memorytypes/entity/ObjectInstance.h>
#include <MemoryX/libraries/memorytypes/MemoryXTypesObjectFactories.h>
#include <MemoryX/core/MemoryXCoreObjectFactories.h>
#include <VirtualRobot/XML/RobotIO.h>
#include <VirtualRobot/RobotNodeSet.h>
#include <RobotAPI/libraries/core/Pose.h>
#include <MemoryX/libraries/helpers/VirtualRobotHelpers/SimoxObjectWrapper.h>
#include <ArmarXCore/core/system/cmake/CMakePackageFinder.h>
#include <ArmarXCore/core/system/ArmarXDataPath.h>

#include <RobotComponents/components/MotionPlanning/CSpace/SimoxCSpace.h>
#include <RobotComponents/components/MotionPlanning/CSpace/ScaledCSpace.h>
#include <RobotComponents/components/MotionPlanning/Tasks/RRTConnect/Task.h>
#include <RobotComponents/components/MotionPlanning/Tasks/AStar/Task.h>
#include <RobotComponents/components/MotionPlanning/Tasks/RandomShortcutPostprocessor/Task.h>

void armarx::MotionPlanningKitchenExampleComponent::onInitComponent()
{
    pServerName = getProperty<std::string>("PlanningServerName").getValue();
    wMemName = getProperty<std::string>("WorkingMemoryName").getValue();

    usingProxy(pServerName);
    usingProxy(wMemName);
}

void armarx::MotionPlanningKitchenExampleComponent::onConnectComponent()
{

    wMemProxy = getProxy<memoryx::WorkingMemoryInterfacePrx>(wMemName);
    pServerProxy = getProxy<MotionPlanningServerInterfacePrx>(pServerName);

    classesSegmentPrx = wMemProxy->getPriorKnowledge()->getObjectClassesSegment();
    cStorageProxy = wMemProxy->getCommonStorage();

    if (!classesSegmentPrx)
    {
        ARMARX_ERROR << "no classes segment";
        return;
    }
    if (!cStorageProxy)
    {
        ARMARX_ERROR << "common storage";
        return;
    }

    runGrasp(false);
    runGrasp(true);
    runWalk();
    ARMARX_IMPORTANT << "done";
}

armarx::CSpaceBasePtr armarx::MotionPlanningKitchenExampleComponent::createCSpaceForGrasping(bool minimal)
{
    SimoxCSpacePtr cspace = new SimoxCSpace(cStorageProxy);

    //agent
    {
        AgentPlanningInformation agentData;

        //the agents position in the space
        agentData.agentPose = new Pose {Eigen::Matrix3f::Identity(), Eigen::Vector3f { 2900, 3165, 0}};
        //define information required to load the agent
        agentData.agentProjectName = "RobotAPI";
        agentData.agentRelativeFilePath = "RobotAPI/robots/Armar3/ArmarIII.xml";
        //define chains for collision checking
        agentData.collisionSetNames.emplace_back("PlatformTorsoHeadColModel");
        agentData.collisionSetNames.emplace_back("RightArmHandColModel");
        agentData.collisionSetNames.emplace_back("LeftArmHandColModel");
        //define chains for movement planning
        agentData.kinemaicChainNames.emplace_back("LeftArm"); // has 7 dof
        agentData.kinemaicChainNames.emplace_back("RightArm"); // has 7 dof
        //add node for movement planning
        agentData.additionalJointsForPlanning.emplace_back("Hip Pitch");//+1 dof
        //remove some nodes from movement planning
        agentData.jointsExcludedFromPlanning.emplace_back("Shoulder 2 R"); //-1 dof
        agentData.jointsExcludedFromPlanning.emplace_back("Shoulder 2 L"); //-1 dof
        // => 13 dof

        //the agent is set for usage
        cspace->setAgent(agentData);
    }

    //add a bowl to the scene
    {
        StationaryObject obj;
        obj.objectClassBase = memoryx::ObjectClassBasePtr::dynamicCast(classesSegmentPrx->getEntityByName("orangebowl"));
        //the table is at its default pose
        Eigen::Matrix4f pose = Eigen::Matrix4f::Identity();
        pose(0, 3) = 2900;
        pose(1, 3) = 3700;
        pose(2, 3) = 830;
        // -90° around x
        pose(1, 1) = 0;
        pose(1, 2) = 1;
        pose(2, 1) = -1;
        pose(2, 2) = 0;
        obj.objectPose =  new Pose {pose};
        //add it to the cspace
        cspace->addStationaryObject(obj);
    }

    if (minimal)
    {
        //only add desk

        //pass all objects from the scene to the cspace
        memoryx::ObjectInstanceMemorySegmentBasePrx objInstPrx = wMemProxy->getObjectInstancesSegment();

        std::string name {"controltable"};
        const memoryx::EntityBasePtr entityBase = objInstPrx->getEntityByName(name);
        if (!entityBase)
        {
            ARMARX_ERROR << "no entity controltable in the working memory! Did you start the simulation and loaded the correct snapshot?";
            throw std::logic_error {"no entity controltable in the working memory! Did you start the simulation and loaded the correct snapshot?"};
        }

        const memoryx::ObjectInstanceBasePtr object = memoryx::ObjectInstancePtr::dynamicCast(entityBase);

        const std::string className = object->getMostProbableClass();
        memoryx::ObjectClassList classes = wMemProxy->getPriorKnowledge()->getObjectClassesSegment()->getClassWithSubclasses(className);

        if (!classes.size())
        {
            ARMARX_INFO << "No classes for most probable class '" << className << "' of object '" << object->getName() << "' with id name " << name;
        }

        cspace->addStationaryObject(
        {
            classes.at(0),
            armarx::PoseBasePtr{new armarx::Pose{object->getPositionBase(), object->getOrientationBase()}}
        });
        return cspace;
    }
    cspace->addObjectsFromWorkingMemory(wMemProxy);
    return cspace;
}

armarx::CSpaceBasePtr armarx::MotionPlanningKitchenExampleComponent::createCSpaceForWalking()
{
    SimoxCSpaceWith2DPosePtr cspace = new SimoxCSpaceWith2DPose(cStorageProxy);

    //agent
    {
        AgentPlanningInformation agentData;

        //the agents position in the space
        agentData.agentPose = new Pose {};
        //define information required to load the agent
        agentData.agentProjectName = "RobotAPI";
        agentData.agentRelativeFilePath = "RobotAPI/robots/Armar3/ArmarIII.xml";
        //define chains for collision checking
        agentData.collisionSetNames.emplace_back("PlatformTorsoHeadColModel");
        agentData.collisionSetNames.emplace_back("RightArmHandColModel");
        agentData.collisionSetNames.emplace_back("LeftArmHandColModel");
        //define chains for movement planning
        //none

        //attach object
        AttachedObject attached;
        attached.attachedToRobotNodeName = "Platform";
        attached.associatedCollisionSetName = "PlatformTorsoHeadColModel";
        memoryx::EntityBasePtr classesEntity = classesSegmentPrx->getEntityByName("orangebowl");
        attached.objectClassBase = memoryx::ObjectClassBasePtr::dynamicCast(classesEntity);
        Eigen::Matrix4f pose = Eigen::Matrix4f::Identity();
        pose(0, 3) = 0;
        pose(1, 3) = 3700 - 3165;
        pose(2, 3) = 830;
        // -90° around x
        pose(1, 1) = 0;
        pose(1, 2) = 1;
        pose(2, 1) = -1;
        pose(2, 2) = 0;
        attached.relativeObjectPose = new Pose {pose};
        agentData.attachedObjects.emplace_back(attached);

        agentData.initialJointValues["Upperarm L"] = 0.5;
        agentData.initialJointValues["Elbow L"] = -0.4;
        agentData.initialJointValues["Underarm L"] = 0.7;
        agentData.initialJointValues["Upperarm R"] = 0.5;
        agentData.initialJointValues["Elbow R"] = -0.4;
        agentData.initialJointValues["Underarm R"] = 0.7;

        //the agent is set for usage
        cspace->setAgent(agentData);
    }
    Vector3fRange poseBounds;
    poseBounds.min.e0 = 1000;
    poseBounds.min.e1 = 1000;
    poseBounds.min.e2 = -M_PI;
    poseBounds.max.e0 =  5000;
    poseBounds.max.e1 =  10000;
    poseBounds.max.e2 =  M_PI;
    cspace->setPoseBounds(poseBounds);
    cspace->addObjectsFromWorkingMemory(wMemProxy);
    return cspace;
}

void armarx::MotionPlanningKitchenExampleComponent::runGrasp(bool minimal)
{
    ARMARX_IMPORTANT << "now planning grasping using a " << (minimal ? std::string {"reduced"} : std::string {"full"}) << " scene";
    armarx::CSpaceBasePtr cspace = createCSpaceForGrasping(minimal);

    PathCollectionHandle& taskHandle = minimal ? graspBowlMin : graspBowl;

    VectorXf start(13);
    start.at(2) = -1.5;
    start.at(8) = -1.5;

    VectorXf goal(13);
    goal.at(1) = 0.5; //Upperarm L
    goal.at(2) = -0.4; //Elbow L
    goal.at(3) = 0.7; //Underarm L
    goal.at(7) = 0.5; //Upperarm R
    goal.at(8) = -0.4; //Elbow R
    goal.at(9) = 0.7; //Underarm R

    //this task will store all results so we can compare them
    PathCollectionPtr pcol = new PathCollection("Results_runGrasp_minimal=" + to_string(minimal));
    pcol->cspace = cspace;

    MotionPlanningTaskBasePtr rrt = new RRTConnectTask {cspace, start, goal};

    ARMARX_IMPORTANT << "Running RRTConnect";
    std::chrono::high_resolution_clock::time_point tstart = std::chrono::high_resolution_clock::now();
    RRTConnectTaskHandle rrtHandle = pServerProxy->enqueueTask(MotionPlanningTaskBasePtr::dynamicCast(rrt));
    rrtHandle->waitForFinishedRunning();
    std::chrono::high_resolution_clock::time_point tend = std::chrono::high_resolution_clock::now();
    ARMARX_IMPORTANT << "RRTConnect took " << std::chrono::duration_cast<std::chrono::milliseconds>(tend - tstart).count() << " ms";

    Path pr = rrtHandle->getPath();
    pr.pathName = "rrt_orig";
    pcol->addPath(pr);

    ARMARX_IMPORTANT << "now optimizing the path for 1, 2, 5, and 10 seconds";
    //refine for {1, 2, 5, 10} second[s]
    pcol->maximalPlanningTimeInSeconds = 1;
    RandomShortcutPostprocessorTaskHandle pp1 = pServerProxy->enqueueTask(new RandomShortcutPostprocessorTask {pcol});

    pcol->maximalPlanningTimeInSeconds = 2;
    RandomShortcutPostprocessorTaskHandle pp2 = pServerProxy->enqueueTask(new RandomShortcutPostprocessorTask {pcol});

    pcol->maximalPlanningTimeInSeconds = 5;
    RandomShortcutPostprocessorTaskHandle pp5 = pServerProxy->enqueueTask(new RandomShortcutPostprocessorTask {pcol});

    pcol->maximalPlanningTimeInSeconds = 10;
    RandomShortcutPostprocessorTaskHandle pp10 = pServerProxy->enqueueTask(new RandomShortcutPostprocessorTask {pcol});

    pp10->waitForFinishedRunning();


    PathWithCost p1 = pp1->getPathWithCost();
    p1.pathName = "smooth_for_1_sec";
    pcol->addPath(p1);
    PathWithCost p2 = pp2->getPathWithCost();
    p2.pathName = "smooth_for_2_sec";
    pcol->addPath(p2);
    PathWithCost p5 = pp5->getPathWithCost();
    p5.pathName = "smooth_for_5_sec";
    pcol->addPath(p5);
    PathWithCost p10 = pp10->getPathWithCost();
    p10.pathName = "smooth_for_10_sec";
    pcol->addPath(p10);

    taskHandle = pServerProxy->enqueueTask(MotionPlanningTaskBasePtr::dynamicCast(pcol));
    ARMARX_IMPORTANT << "adding PathCollection containing all data\n"
                     << "Ice identity = " << taskHandle->ice_getIdentity().name << "\n"
                     << "Path 1 = RRTConnect\n"
                     << "Path 2 = after optimizing 1 second\n"
                     << "Path 3 = after optimizing 2 seconds\n"
                     << "Path 4 = after optimizing 5 seconds\n"
                     << "Path 5 = after optimizing 10 seconds\n";
}

void armarx::MotionPlanningKitchenExampleComponent::runWalk()
{
    ARMARX_IMPORTANT << "now planning walking";
    CSpaceBasePtr cspace = createCSpaceForWalking();
    PathCollectionHandle& taskHandle = walk;

    //this task will store all results so we can compare them
    PathCollectionPtr pcol = new PathCollection("Results_runWalk");

    VectorXf start { 2900, 3165, 0};
    VectorXf goal  { 2900, 9370, 0};

    ScaledCSpacePtr scaledCspace = new ScaledCSpace {cspace, {0.005, 0.005, 1}};
    scaledCspace->scaleConfig(start);
    scaledCspace->scaleConfig(goal);
    pcol->cspace = scaledCspace;

    float dcdStepSize = 0.1;

    //create the task
    MotionPlanningTaskBasePtr taskAStar = new  armarx::AStarTask {scaledCspace, start, goal, "runWalkAStar", dcdStepSize};
    ARMARX_IMPORTANT << "Running A*";
    const std::chrono::high_resolution_clock::time_point astarStart = std::chrono::high_resolution_clock::now();
    AStarTaskHandle astarHandle = pServerProxy->enqueueTask(taskAStar);
    astarHandle->waitForFinishedRunning();
    const std::chrono::high_resolution_clock::time_point astarStop = std::chrono::high_resolution_clock::now();
    Ice::Long astarRunT = std::chrono::duration_cast<std::chrono::milliseconds>(astarStop - astarStart).count();
    ARMARX_IMPORTANT << "AStar took " << astarRunT << " ms";
    long rrtMaxRunTime = astarRunT / 1000;

    Path pa = astarHandle->getPath();
    pa.pathName = "astar";
    pcol->addPath(pa);

    //rrt with n workers
    auto runRRT = [&](int workerCnt)
    {
        MotionPlanningTaskBasePtr taskRRT = new RRTConnectTask {scaledCspace, start, goal, "runWalkRRT", rrtMaxRunTime, dcdStepSize, workerCnt};
        ARMARX_IMPORTANT << "Now running RRTConnect (" << workerCnt << " worker) + RandomShortcutPostprocessing for the same time";
        const std::chrono::high_resolution_clock::time_point rrtStart = std::chrono::high_resolution_clock::now();
        RandomShortcutPostprocessorTaskHandle rrtHandle = pServerProxy->enqueueTask(new RandomShortcutPostprocessorTask {taskRRT});
        rrtHandle->waitForFinishedPlanning();
        const std::chrono::high_resolution_clock::time_point rrtPlanStop = std::chrono::high_resolution_clock::now();
        Ice::Long rrtRunT = std::chrono::duration_cast<std::chrono::milliseconds>(rrtPlanStop - rrtStart).count();
        ARMARX_IMPORTANT << "RRTConnect (" << workerCnt << " worker) took " << rrtRunT << " ms to find a path. Now searching for shortcuts.";
        rrtHandle->waitForFinishedRunning();
        PathWithCost p1 = rrtHandle->getNthPathWithCost(1);
        p1.pathName = "RRT_orig_#worker=" + to_string(workerCnt);
        pcol->addPath(std::move(p1));
        PathWithCost p2 = rrtHandle->getNthPathWithCost(0);
        p2.pathName = "Smooth_#worker=" + to_string(workerCnt);
        pcol->addPath(std::move(p2));
    };

    //rrt with 4 workers
    runRRT(4);
    //rrt with 1 worker
    runRRT(1);
    taskHandle = pServerProxy->enqueueTask(MotionPlanningTaskBasePtr::dynamicCast(pcol));
    ARMARX_IMPORTANT << "adding PathCollection containing all data\n"
                     << "Ice identity = " << taskHandle->ice_getIdentity().name << "\n"
                     << "Path 1 = A*\n"
                     << "Path 2 = RRTConnect(4)\n"
                     << "Path 3 = RRTConnect(4) after optimizing the remaining time\n"
                     << "Path 4 = RRTConnect(1)\n"
                     << "Path 5 = RRTConnect(1) after optimizing the remaining time\n";
}

