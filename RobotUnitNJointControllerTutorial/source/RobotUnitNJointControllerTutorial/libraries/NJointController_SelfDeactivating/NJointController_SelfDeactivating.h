/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotUnitNJointControllerTutorial::ArmarXObjects::NJointController_SelfDeactivating
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2018
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#pragma once

#include <RobotAPI/components/units/RobotUnit/RobotUnit.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTargetBase.h>

namespace armarx
{
    TYPEDEF_PTRS_HANDLE(NJointController_SelfDeactivating);
    TYPEDEF_PTRS_HANDLE(NJointController_SelfDeactivating_Config);

    /**
    * @ingroup RobotUnitNJointControllerTutorial
    * @brief This controller deactivates it self after 100 iterations.
    * This feature can be used to shut down a controller and signal an exceptional state,
    * without throwing an exception
    */
    class NJointController_SelfDeactivating_Config : virtual public NJointControllerConfig
    {
    public:
        NJointController_SelfDeactivating_Config(const std::string& deviceName) : deviceName{deviceName} {}
        std::string deviceName;
    };

    class NJointController_SelfDeactivating: public NJointController
    {
    public:
        static WidgetDescription::WidgetPtr GenerateConfigDescription
        (
            const VirtualRobot::RobotPtr&,
            const std::map<std::string, ConstControlDevicePtr>& controlDevices,
            const std::map<std::string, ConstSensorDevicePtr>& sensorDevices
        );
        static NJointControllerConfigPtr GenerateConfigFromVariants(const StringVariantBaseMap& values)
        {
            return new NJointController_SelfDeactivating_Config{values.at("name")->getString()};
        }
        NJointController_SelfDeactivating(RobotUnitPtr prov, NJointControllerConfigPtr config, const VirtualRobot::RobotPtr&);

        std::string getClassName(const Ice::Current&) const override
        {
            return "NJointController_SelfDeactivating";
        }

        void rtRun(const IceUtil::Time& /*sensorValuesTimestamp*/, const IceUtil::Time& /*timeSinceLastIteration*/) override
        {
            if (100 < ++count)
            {
                rtSetErrorState(); // this will stop the controller
            }
            ARMARX_RT_LOGF_IMPORTANT("count = %u", count);
            *target = 0;
        }

    protected:
        void rtPreActivateController() override
        {
            count = 0;
        }

        float* target;
        std::size_t count;
    };
}
