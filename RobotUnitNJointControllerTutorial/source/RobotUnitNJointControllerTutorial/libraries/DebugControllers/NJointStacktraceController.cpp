/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotUnitNJointControllerTutorial::ArmarXObjects::NJointStacktraceController
 * @author     Raphael ( ufdrv at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "NJointStacktraceController.h"
#include <ArmarXGui/libraries/DefaultWidgetDescriptions/DefaultWidgetDescriptions.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTarget1DoFActuator.h>
#include <RobotAPI/components/units/RobotUnit/ControlTargets/ControlTargetHolonomicPlatformVelocity.h>

using namespace armarx;

WidgetDescription::WidgetPtr NJointStacktraceController::GenerateConfigDescription(const VirtualRobot::RobotPtr&, const std::map<std::string, ConstControlDevicePtr>& controlDevices, const std::map<std::string, ConstSensorDevicePtr>& sensorDevices)
{
    ARMARX_IMPORTANT_S << LogSender::CreateBackTrace();
    using namespace armarx::WidgetDescription;
    FormLayoutPtr layout = new FormLayout;
    StringComboBoxPtr boxDevice = new StringComboBox;
    StringComboBoxPtr boxMode = new StringComboBox;
    boxDevice->name = "name";
    boxMode->name = "mode";
    boxDevice->options.reserve(controlDevices.size());
    std::set<std::string> modes;
    for (const auto& pair : controlDevices)
    {
        const ConstControlDevicePtr& dev = pair.second;
        modes.insert(dev->getControlModes().begin(), dev->getControlModes().end());
        boxDevice->options.emplace_back(pair.first);
    }
    boxMode->options.assign(modes.begin(), modes.end());
    layout->children.emplace_back(makeFormLayoutElement("ControlDevice", boxDevice));
    LineEditPtr edit = new LineEdit;
    edit->defaultValue = ControlModes::Position1DoF;
    edit->name = "mode";
    layout->children.emplace_back(makeFormLayoutElement("ControlMode", edit));
    FloatSpinBoxPtr spamDelay = new FloatSpinBox;
    spamDelay->defaultValue = 10;
    spamDelay->name = "spamDelay";
    spamDelay->min = 0;
    spamDelay->max = 100;
    layout->children.emplace_back(makeFormLayoutElement("spam delay", spamDelay));
    return layout;
}

NJointStacktraceControllerConfigPtr NJointStacktraceController::GenerateConfigFromVariants(const StringVariantBaseMap& values)
{
    ARMARX_IMPORTANT_S << LogSender::CreateBackTrace();
    return new NJointStacktraceControllerConfig {values.at("name")->getString(), values.at("mode")->getString(), values.at("spamDelay")->getFloat()};
}

NJointStacktraceController::NJointStacktraceController(RobotUnitPtr prov, NJointStacktraceControllerConfigPtr cfg, const VirtualRobot::RobotPtr&)
{
    ARMARX_CHECK_EXPRESSION(prov);
    spamDelay = cfg->spamDelay;
    ControlTargetBase* ct = useControlTarget(cfg->deviceName, cfg->controlMode);
    //get sensor
    if (cfg->controlMode == ControlModes::Position1DoF)
    {
        ARMARX_CHECK_EXPRESSION(ct->asA<ControlTarget1DoFActuatorPosition>());
        auto val = &(ct->asA<ControlTarget1DoFActuatorPosition>()->position);
        run = [val] {*val = 0;};
    }
    else if (cfg->controlMode == ControlModes::Velocity1DoF)
    {
        ARMARX_CHECK_EXPRESSION(ct->asA<ControlTarget1DoFActuatorVelocity>());
        auto val = &(ct->asA<ControlTarget1DoFActuatorVelocity>()->velocity);
        run = [val] {*val = 0;};
    }
    else if (cfg->controlMode == ControlModes::Torque1DoF)
    {
        ARMARX_CHECK_EXPRESSION(ct->asA<ControlTarget1DoFActuatorTorque>());
        auto val = &(ct->asA<ControlTarget1DoFActuatorTorque>()->torque);
        run = [val] {*val = 0;};
    }
    else if (cfg->controlMode == ControlModes::HolonomicPlatformVelocity)
    {
        ARMARX_CHECK_EXPRESSION(ct->asA<ControlTargetHolonomicPlatformVelocity>());
        ControlTargetHolonomicPlatformVelocity* val = ct->asA<ControlTargetHolonomicPlatformVelocity>();
        run = [val]
        {
            val->velocityRotation = 0;
            val->velocityX = 0;
            val->velocityY = 0;
        };
    }
    else
    {
        throw InvalidArgumentException {"Unsupported control mode: " + cfg->controlMode};
    }
}

std::string NJointStacktraceController::getClassName(const Ice::Current&) const
{
    ARMARX_IMPORTANT << LogSender::CreateBackTrace();
    return "NJointStacktraceController";
}

void NJointStacktraceController::rtRun(const IceUtil::Time&, const IceUtil::Time&)
{
    ARMARX_IMPORTANT << deactivateSpam(spamDelay) << LogSender::CreateBackTrace();
    run();
}

void NJointStacktraceController::onInitNJointController()
{
    ARMARX_IMPORTANT << LogSender::CreateBackTrace();
}

void NJointStacktraceController::onConnectNJointController()
{
    ARMARX_IMPORTANT << LogSender::CreateBackTrace();
}

void NJointStacktraceController::onDisconnectNJointController()
{
    ARMARX_IMPORTANT << LogSender::CreateBackTrace();
}

void NJointStacktraceController::onExitNJointController()
{
    ARMARX_IMPORTANT << LogSender::CreateBackTrace();
}

WidgetDescription::StringWidgetDictionary NJointStacktraceController::getFunctionDescriptions(const Ice::Current&) const
{
    ARMARX_IMPORTANT << LogSender::CreateBackTrace();
    return NJointController::getFunctionDescriptions();
}

void NJointStacktraceController::callDescribedFunction(const std::string& s, const StringVariantBaseMap& v, const Ice::Current&)
{
    ARMARX_IMPORTANT <<  LogSender::CreateBackTrace();
    NJointController::callDescribedFunction(s, v);
}

void NJointStacktraceController::rtPreActivateController()
{
    ARMARX_IMPORTANT <<  LogSender::CreateBackTrace();
    NJointController::rtPreActivateController();
}

void NJointStacktraceController::rtPostDeactivateController()
{
    ARMARX_IMPORTANT << LogSender::CreateBackTrace();
    NJointController::rtPostDeactivateController();
}

void NJointStacktraceController::onPublishActivation(const DebugDrawerInterfacePrx& dr, const DebugObserverInterfacePrx& ob)
{
    ARMARX_IMPORTANT << LogSender::CreateBackTrace();
    NJointController::onPublishActivation(dr, ob);
}

void NJointStacktraceController::onPublishDeactivation(const DebugDrawerInterfacePrx& dr, const DebugObserverInterfacePrx& ob)
{
    ARMARX_IMPORTANT << LogSender::CreateBackTrace();
    NJointController::onPublishDeactivation(dr, ob);
}

void NJointStacktraceController::onPublish(const SensorAndControl& sc, const DebugDrawerInterfacePrx& draw, const DebugObserverInterfacePrx& obs)
{
    ARMARX_IMPORTANT << deactivateSpam(spamDelay) << LogSender::CreateBackTrace();
    NJointController::onPublish(sc, draw, obs);
}

NJointControllerRegistration<NJointStacktraceController> registrationControllerNJointStacktraceController("NJointStacktraceController");
ARMARX_ASSERT_NJOINTCONTROLLER_HAS_CONSTRUCTION_GUI(NJointStacktraceController);
