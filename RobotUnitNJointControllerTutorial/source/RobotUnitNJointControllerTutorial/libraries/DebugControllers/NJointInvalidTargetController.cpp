/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotUnitNJointControllerTutorial::ArmarXObjects::NJointInvalidTargetController
 * @author     Raphael ( ufdrv at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "NJointInvalidTargetController.h"
#include <ArmarXGui/libraries/DefaultWidgetDescriptions/DefaultWidgetDescriptions.h>

using namespace armarx;

std::string NJointInvalidTargetController::getClassName(const Ice::Current&) const
{
    return "NJointInvalidTargetController";
}

void NJointInvalidTargetController::rtRun(const IceUtil::Time&, const IceUtil::Time&)
{
    NJointDebugBaseController::rtRun();
    if (triggered)
    {
        targets.back()->reset();
    }
}

NJointControllerRegistration<NJointInvalidTargetController> registrationControllerNJointInvalidTargetController("NJointInvalidTargetController");
ARMARX_ASSERT_NJOINTCONTROLLER_HAS_CONSTRUCTION_GUI(NJointInvalidTargetController);


