/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    RobotUnitNJointControllerTutorial::ArmarXObjects::NJointPositionPassThroughController
 * @author     Raphael ( ufdrv at student dot kit dot edu )
 * @date       2017
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include "NJointPositionPassThroughController.h"
#include <RobotAPI/libraries/core/Pose.h>

using namespace armarx;

WidgetDescription::WidgetPtr NJointPositionPassThroughController::GenerateConfigDescription(const VirtualRobot::RobotPtr&, const std::map<std::string, ConstControlDevicePtr>& controlDevices, const std::map<std::string, ConstSensorDevicePtr>& sensorDevices)
{
    using namespace armarx::WidgetDescription;
    HBoxLayoutPtr layout = new HBoxLayout;
    LabelPtr label = new Label;
    label->text = "control device name";
    layout->children.emplace_back(label);
    StringComboBoxPtr box = new StringComboBox;
    box->defaultIndex = 0;
    //filter control devices
    for (const auto& name2dev : controlDevices)
    {
        const ConstControlDevicePtr& dev = name2dev.second;
        const auto& name = name2dev.first;
        if (
            dev->hasJointController(ControlModes::Position1DoF) &&
            dev->getJointController(ControlModes::Position1DoF)->getControlTarget()->isA<ControlTarget1DoFActuatorPosition>() &&
            sensorDevices.count(name) &&
            sensorDevices.at(name)->getSensorValue()->isA<SensorValue1DoFActuatorPosition>()
        )
        {
            box->options.emplace_back(name);
        }
    }
    box->name = "name";
    layout->children.emplace_back(box);
    return layout;
}

NJointControllerConfigPtr NJointPositionPassThroughController::GenerateConfigFromVariants(const StringVariantBaseMap& values)
{
    //you should check here for types null additional params etc
    return new NJointPositionPassThroughControllerConfig {values.at("name")->getString()};
}

NJointPositionPassThroughController::NJointPositionPassThroughController(RobotUnitPtr prov, NJointControllerConfigPtr config, const VirtualRobot::RobotPtr&)
{
    ARMARX_CHECK_EXPRESSION(prov);
    NJointPositionPassThroughControllerConfigPtr cfg = NJointPositionPassThroughControllerConfigPtr::dynamicCast(config);
    ARMARX_CHECK_EXPRESSION_W_HINT(cfg, "The provided config has the wrong type! The type is " << config->ice_id());
    const SensorValueBase* sv = useSensorValue(cfg->deviceName);
    ControlTargetBase* ct = useControlTarget(cfg->deviceName, ControlModes::Position1DoF);
    ARMARX_CHECK_EXPRESSION(sv->asA<SensorValue1DoFActuatorPosition>());
    ARMARX_CHECK_EXPRESSION(ct->asA<ControlTarget1DoFActuatorPosition>());
    sensor = sv->asA<SensorValue1DoFActuatorPosition>();
    target = ct->asA<ControlTarget1DoFActuatorPosition>();
}

std::string NJointPositionPassThroughController::getClassName(const Ice::Current&) const
{
    return "NJointPositionPassThroughController";
}

WidgetDescription::StringWidgetDictionary NJointPositionPassThroughController::getFunctionDescriptions(const Ice::Current&) const
{
    using namespace armarx::WidgetDescription;
    HBoxLayoutPtr layoutSetPos = new HBoxLayout;
    {
        LabelPtr label = new Label;
        label->text = "positiontarget";
        layoutSetPos->children.emplace_back(label);
        FloatSpinBoxPtr spin = new FloatSpinBox;
        spin->defaultValue = 0;
        spin->max = 1;
        spin->min = -1;
        spin->name = "spinmearound";
        layoutSetPos->children.emplace_back(spin);
    }
    VBoxLayoutPtr layoutSetHalfPos = new VBoxLayout;
    {
        LabelPtr label = new Label;
        label->text = "positiontarget / 2";
        layoutSetHalfPos->children.emplace_back(label);
        FloatSpinBoxPtr spin = new FloatSpinBox;
        spin->defaultValue = 0;
        spin->max = 0.5;
        spin->min = -0.5;
        spin->name = "spinmehalfaround";
        layoutSetHalfPos->children.emplace_back(spin);
    }
    return {{"SetPosition", layoutSetPos}, {"SetPositionHalf", layoutSetHalfPos}};
}

void NJointPositionPassThroughController::callDescribedFunction(const std::string& name, const StringVariantBaseMap& value, const Ice::Current&)
{
    if (name == "SetPosition")
    {
        //you should check here for types null additional params etc
        targetPos = value.at("spinmearound")->getFloat();
    }
    else if (name == "SetPositionHalf")
    {
        //you should check here for types null additional params etc
        targetPos = value.at("spinmehalfaround")->getFloat() * 2;
    }
    else
    {
        ARMARX_ERROR << "CALLED UNKNOWN REMOTE FUNCTION: " << name;
    }
}

void NJointPositionPassThroughController::rtRun(const IceUtil::Time& t, const IceUtil::Time&)
{
    ARMARX_RT_LOGF_ERROR("A MESSAGE PARAMETER %f", t.toSecondsDouble()).deactivateSpam(1);
    ARMARX_RT_LOGF_IMPORTANT("A MESSAGE WITHOUT PARAMETERS").deactivateSpam(1);
    target->position = targetPos;
    currentPos = sensor->position;
}

void NJointPositionPassThroughController::onPublishActivation(const DebugDrawerInterfacePrx&, const DebugObserverInterfacePrx&)
{
    //we could do some setup for the drawer here. e.g. add a robot
}

void NJointPositionPassThroughController::onPublishDeactivation(const DebugDrawerInterfacePrx& drawer, const DebugObserverInterfacePrx&)
{
    drawer->removeLayer("Layer_" + getInstanceName());
}

void NJointPositionPassThroughController::onPublish(const SensorAndControl&, const DebugDrawerInterfacePrx& drawer, const DebugObserverInterfacePrx&)
{
    drawer->setSphereVisu("Layer_" + getInstanceName(), "positionball", new Vector3, armarx::DrawColor {1, 1, 1, 1}, currentPos * 2000);
}

NJointControllerRegistration<NJointPositionPassThroughController> registrationControllerNJointPositionPassThroughController("NJointPositionPassThroughController");
ARMARX_ASSERT_NJOINTCONTROLLER_HAS_CONSTRUCTION_GUI(NJointPositionPassThroughController);





