/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SendingCustomTypesViaIce::ArmarXObjects::EigenTypeLooper
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2020
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <thread>

#include "EigenTypeLooper.h"

namespace armarx
{
    std::string EigenTypeLooper::getDefaultName() const
    {
        return "EigenTypeLooper";
    }

    void EigenTypeLooper::v3(const Eigen::Vector3f& v3, const Ice::Current&)
    {
        ARMARX_IMPORTANT << "v3    "
                         << v3.transpose();
    }
    void EigenTypeLooper::q(const Eigen::Quaterniond& q, const Ice::Current&)
    {
        ARMARX_IMPORTANT << "q    "
                         << q.w() << " " << q.x() << " "
                         << q.y() << " " << q.z();
    }
    void EigenTypeLooper::vx(const Eigen::VectorXf& vx, const Ice::Current&)
    {
        ARMARX_IMPORTANT << "vx    " << vx.transpose();
    }
    void EigenTypeLooper::rvx(const Eigen::RowVectorXf& vx, const Ice::Current&)
    {
        ARMARX_IMPORTANT << "rvx   " << vx;
    }
    void EigenTypeLooper::mX(const Eigen::MatrixXf& mX, const Ice::Current&)
    {
        ARMARX_IMPORTANT << "mX\n" << mX;
    }
    void EigenTypeLooper::m2X(const Eigen::Matrix2Xf& mX, const Ice::Current&)
    {
        ARMARX_IMPORTANT << "m2X\n" << mX;
    }
    void EigenTypeLooper::mX2(const Eigen::MatrixX2f& mX, const Ice::Current&)
    {
        ARMARX_IMPORTANT << "mX2\n" << mX;
    }

    void EigenTypeLooper::onInitComponent()
    {
        usingTopic("topic");
        offeringTopic("topic");
    }

    void EigenTypeLooper::onConnectComponent()
    {
        topic = getTopic<EigenTypeLooperInterfacePrx>("topic");
        std::thread{[this]{
                std::uint8_t i = 0;
                while (true)
                {
                    std::this_thread::sleep_for(std::chrono::seconds{1});
                    ARMARX_TRACE;
                    if (i % 7 == 0)
                    {
                        ARMARX_INFO << "send " << static_cast<int>(i) << " v3";
                        topic->v3(Eigen::Vector3f::Constant(i));
                    }
                    else if (i % 7 == 1)
                    {
                        ARMARX_INFO << "send " << static_cast<int>(i) << " q";
                        ::Eigen::Quaterniond q;
                        q.x() = i;
                        q.y() = i;
                        q.z() = i;
                        q.w() = i;
                        topic->q(q);
                    }
                    else if (i % 7 == 2)
                    {
                        ARMARX_INFO << "send " << static_cast<int>(i) << " vx";
                        ::Eigen::VectorXf vx(i % 8);
                        vx.setConstant(i);
                        topic->vx(vx);
                    }
                    else if (i % 7 == 3)
                    {
                        ARMARX_INFO << "send " << static_cast<int>(i) << " rvx";
                        ::Eigen::RowVectorXf vx(i % 8);
                        vx.setConstant(i);
                        topic->rvx(vx);
                    }
                    else if (i % 7 == 4)
                    {
                        ARMARX_INFO << "send " << static_cast<int>(i) << " mX";
                        ::Eigen::MatrixXf vx(i % 8, i % 8);
                        vx.setConstant(i);
                        topic->mX(vx);
                    }
                    else if (i % 7 == 5)
                    {
                        ARMARX_INFO << "send " << static_cast<int>(i) << " m2X";
                        ::Eigen::Matrix2Xf vx(2, i % 8);
                        vx.setConstant(i);
                        topic->m2X(vx);
                    }
                    else if (i % 7 == 6)
                    {
                        ARMARX_INFO << "send " << static_cast<int>(i) << " mX2";
                        ::Eigen::MatrixX2f vx(i % 8, 2);
                        vx.setConstant(i);
                        topic->mX2(vx);
                    }
                    ++i;
                }
            }}.detach();
    }


    armarx::PropertyDefinitionsPtr EigenTypeLooper::createPropertyDefinitions()
    {
        return armarx::PropertyDefinitionsPtr(new ComponentPropertyDefinitions(
                getConfigIdentifier()));
    }
}
