/*
 * This file is part of ArmarX.
 *
 * ArmarX is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 *
 * ArmarX is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    SendingCustomTypesViaIce::ArmarXObjects::CustomTypeLooper
 * @author     Raphael Grimm ( raphael dot grimm at kit dot edu )
 * @date       2019
 * @copyright  http://www.gnu.org/licenses/gpl-2.0.txt
 *             GNU General Public License
 */

#include <thread>

#include "CustomTypeLooper.h"


using namespace armarx;


void CustomTypeLooper::onInitComponent()
{
    usingTopic("topic");
    offeringTopic("topic");
}


void CustomTypeLooper::onConnectComponent()
{

    topic = getTopic<CustomTypeLooperInterfacePrx>("topic");
    std::thread{[this]{
            Eigen::Vector3f v3 = Eigen::Vector3f::Zero();
            Eigen::Matrix<float,  6,  6> mx;
            mx << 11, 12, 13, 14, 15, 16,
               21, 22, 23, 24, 25, 26,
               31, 32, 33, 34, 35, 36,
               41, 42, 43, 44, 45, 46,
               51, 52, 53, 54, 55, 56,
               61, 62, 63, 64, 65, 66;
            custom_struct_via_ice_1 c1;
            c1.i = 42;
            c1.d = 84;
            c1.f = 84.168;
            custom_struct_via_ice_2 c2;
            c2.i = 42;
            c2.d = 84;
            c2.f = 84.168;
            c2.s.emplace_back("string\n01234\t56789\nstring");
            c2.s.emplace_back("qwer");
            while (true)
            {
                ++v3(0);
                v3(1) = static_cast<unsigned>(v3(0)) % 42;
                v3(2) = v3(0) / 42;
                std::this_thread::sleep_for(std::chrono::milliseconds{100});
                ARMARX_IMPORTANT_S << "send \t" << v3.transpose();
                topic->receive(v3, mx, c1, c2, 42, "string");
            }
        }}.detach();
}

void CustomTypeLooper::receive(const Eigen::Vector3f& v3,
                               const ::Eigen::Matrix6f& mx,
                               const ::armarx::custom_struct_via_ice_1ada& c1,
                               const ::armarx::custom_struct_via_ice_2ada& c2,
                               const ::boost::variant<int, std::string>& isv1,
                               const ::boost::variant<int, std::string>& isv2,
                               const Ice::Current&)
{
    ARMARX_IMPORTANT << ".                                 got \t" << v3.transpose()
                     << "\n" << mx
                     << "\n" << c1.d << "\t" << c1.f << "\t" << c1.i
                     << "\n" << c2.d << "\t" << c2.f << "\t" << c2.i << "\t" << c2.s
                     << "\nvariants " << boost::get<const int>(isv1) << "\t" << boost::get<const std::string>(isv2);
}



armarx::PropertyDefinitionsPtr CustomTypeLooper::createPropertyDefinitions()
{
    return armarx::PropertyDefinitionsPtr(new ComponentPropertyDefinitions(
            getConfigIdentifier()));
}

