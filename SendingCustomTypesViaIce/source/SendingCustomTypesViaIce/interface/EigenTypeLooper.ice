#pragma once

#include <ArmarXCore/interface/serialization/Eigen.ice>

module armarx
{
    interface EigenTypeLooperInterface
    {
        void v3(::Eigen::Vector3f v3);
        void q(::Eigen::Quaterniond q);
        void vx(::Eigen::VectorXf vx);
        void rvx(::Eigen::RowVectorXf vx);
        void mX(::Eigen::MatrixXf vx);
        void m2X(::Eigen::Matrix2Xf vx);
        void mX2(::Eigen::MatrixX2f vx);
    };
};
