#pragma once

#include <ArmarXCore/interface/serialization/BoostFusionAdaptor.h>

#include <string>
#include <vector>

namespace armarx
{
    //these types could also be defined in a different library the interface lib is linking against
    struct custom_struct_via_ice_1
    {
        int i;
        double d;
        float f;
    };
    struct custom_struct_via_ice_2
    {
        int i;
        double d;
        float f;
        std::vector<std::string> s;
    };
}

ARMARX_MAKE_STRUCT_AND_FUSION_ADAPTOR_CPP(
    ::armarx::custom_struct_via_ice_1,
    (int, i)(double, d)(float, f))

ARMARX_MAKE_STRUCT_AND_FUSION_ADAPTOR_CPP(
    ::armarx::custom_struct_via_ice_2,
    (int, i)(double, d)(float, f)(std::vector<std::string>, s))
